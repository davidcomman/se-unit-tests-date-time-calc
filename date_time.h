#ifndef ASSIGNMENT_3_DATETIME_H
#define ASSIGNMENT_3_DATETIME_H
#include <chrono>
#include <string>

class date_time {
public:
    date_time();
    date_time(time_t time);///construct the object with an epoch time
    date_time(const date_time& object);///copy constructor
    date_time(int year, int month, int day, int hours, int minutes, int seconds);///construct the object with a date
    std::string str();///turn the object into a string with default format
    std::string str(std::string format);///turn the object into a string

    time_t advance(const std::chrono::seconds& seconds);///advance time
    time_t step_back(const std::chrono::seconds& seconds);///step back in time

    date_time& operator=(const date_time& other);///assignment operator

    explicit operator time_t() const { return m_date_time; }///cast object to a time_t

private:
    time_t m_date_time;///contains the epoch time
    bool m_error = false;///an error has occurred
    void setDateTime(int year, int month, int day, int hours, int minutes, int seconds);///set the date and time
};


#endif //ASSIGNMENT_3_DATETIME_H
