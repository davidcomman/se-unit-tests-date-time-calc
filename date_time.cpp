#include "date_time.h"
#include <iostream>
#include <chrono>
#include <iomanip>
#include <ctime>
#include <string>
#include <algorithm>


///default constructor
///initialises the object with the current date and time
date_time::date_time() {
    auto date_time = std::chrono::system_clock::now();
    std::time_t c_date_time = std::chrono::system_clock::to_time_t(date_time); //convert to time_t
    m_date_time = c_date_time;
}

///overloaded constructor
///initialise the object with a time_t variable
date_time::date_time(time_t time) {
    if (time < 0) throw std::domain_error("Epoch time underflow");
    if (time > LONG_MAX) throw std::domain_error("Epoch time overflow");
    m_date_time = time;
}

///overloaded constructor
///initialises the object with the specified date and time
date_time::date_time(int year, int month, int day, int hours, int minutes, int seconds) {
    setDateTime(year, month, day, hours, minutes, seconds);
}

///overloaded constructor
///used for copying objects
date_time::date_time(const date_time& object) {
    m_date_time = object.m_date_time;
    m_error = object.m_error;
    std::cout << "date_time::date_time(const date_time& object) [" << str() << "] called" << std::endl;
}

///returns the date and time of the object with the default format of "%Y-%b-%d %H:%M:%S"
std::string date_time::str() {
    return str("%Y-%b-%d %H:%M:%S");
}

///returns the date and time of the object with a specifed format
std::string date_time::str(std::string format) {
    std::tm local = *std::localtime(&m_date_time);

    std::ostringstream os;
    os << std::put_time(&local, format.c_str());   //output the date&time using the specified format
    return os.str();
}

time_t date_time::advance(const std::chrono::seconds& seconds) {
    if(seconds.count() < 0) {
        return step_back(std::chrono::seconds(-seconds.count()));
    }
    if(m_date_time + seconds.count() > LONG_MAX || m_date_time + seconds.count() < m_date_time) throw std::domain_error("Epoch time overflow");
    m_date_time += seconds.count();
    return m_date_time;
}

time_t date_time::step_back(const std::chrono::seconds& seconds) {
    if(seconds.count() < 0) {
        return advance(std::chrono::seconds(-seconds.count()));
    }
    if(m_date_time < seconds.count()) throw std::domain_error("Epoch time underflow");
    m_date_time -= seconds.count();
    return m_date_time;
}

///assignment operator to copy objects
date_time& date_time::operator=(const date_time& other) {
    std::cout << "date_time::operator=(const date_time&) [" << str() << "] called" << std::endl;
    if(this == &other) return *this;
    m_date_time = other.m_date_time;
    m_error = other.m_error;
    return *this;
}

//private:
void date_time::setDateTime(int year, int month, int day, int hours, int minutes, int seconds) {
    date_time d(0);
    if(year < 0)                 throw std::invalid_argument("Invalid year: " + std::to_string(year));
    if(month < 1 || month > 12)     throw std::invalid_argument("Invalid month: " + std::to_string(month));
    if(day > 31 || day < 1)         throw std::invalid_argument("Invalid day: " + std::to_string(day));
    if(hours < 0 || hours > 23)     throw std::invalid_argument("Invalid hours: " + std::to_string(hours));
    if(minutes < 0 || minutes > 59) throw std::invalid_argument("Invalid minutes: " + std::to_string(minutes));
    if(seconds < 0 || seconds > 59) throw std::invalid_argument("Invalid seconds: " + std::to_string(seconds));

    if(year < 1970) throw std::domain_error("Epoch time underflow");


    if(year > 2038)                                                                                throw std::domain_error("Epoch time overflow");
    else if(year == 2038 && month > 1)                                                             throw std::domain_error("Epoch time overflow");
    else if(year == 2038 && month == 1 && day > 19)                                                throw std::domain_error("Epoch time overflow");
    else if(year == 2038 && month == 1 && day == 19 && hours > 4)                                  throw std::domain_error("Epoch time overflow");
    else if(year == 2038 && month == 1 && day == 19 && hours == 4 && minutes > 14)                 throw std::domain_error("Epoch time overflow");
    else if(year == 2038 && month == 1 && day == 19 && hours == 4 && minutes == 14 && seconds > 7) throw std::domain_error("Epoch time overflow");

    std::tm time;

    time.tm_sec = seconds;
    time.tm_min = minutes;
    time.tm_hour = hours;
    time.tm_mday = day;
    time.tm_mon = month - 1;
    time.tm_year = year - 1900;
    time.tm_isdst = -1;

    m_date_time = std::mktime(&time);
}