#include <gtest/gtest.h>
#include <chrono>
#include "../date_time.h"
#include "../date_time.cpp"

///BOUNDARY VALUE ANALYSIS:
TEST(date_time_tests, time_overflow) {

    date_time d1(LONG_MAX);
    date_time d2(LONG_MAX);
    ASSERT_NO_THROW(d1.step_back(std::chrono::seconds(LONG_MAX)));
    ASSERT_THROW(d2.advance(std::chrono::seconds(1)), std::domain_error);
}

TEST(date_time_tests, time_underflow) {
    date_time d1(0);
    date_time d2(0);

    ASSERT_NO_THROW(d1.advance(std::chrono::seconds(LONG_MAX)));
    ASSERT_THROW(d2.step_back(std::chrono::seconds(1)), std::domain_error);
}

///EQUIVALENCE CLASS:

TEST(date_time_tests, construct_object) {
    ///construct from current time:
    ASSERT_NO_THROW(date_time d);
    ///contruct from epoch time:
    ASSERT_THROW(date_time d(-3), std::domain_error);
    ASSERT_NO_THROW(date_time d(1539767434));
    ///contruct from date:
    ASSERT_THROW(date_time d(1850, 1, 3, 0, 0, 0), std::domain_error);
    ASSERT_NO_THROW(date_time d(1999, 7, 10, 0, 0, 0));
    ///construct from other object:
    date_time d;
    date_time d2 = d;
    ASSERT_EQ((time_t)d, (time_t)d2);
}

TEST(date_time_tests, assign_object) {
    date_time d;
    date_time d2(0);
    d2 = d;
    ASSERT_EQ((time_t)d, (time_t)d2);
}

TEST(date_time_tests, manipulate_time) {
    date_time d(0);
    d.advance(std::chrono::seconds(5));
    date_time d2(1000000);
    d2.step_back(std::chrono::seconds(1000));
    ASSERT_EQ((time_t)d, 5);
    ASSERT_EQ((time_t)d2, 999000);
}

TEST(date_time_tests, conv_str) {
    date_time d(0);
    ASSERT_STREQ(d.str().c_str(), "1970-Jan-01 01:00:00");
    ASSERT_STREQ(d.str("%b-%Y-%d %H:%M:%S").c_str(), "Jan-1970-01 01:00:00");
    ASSERT_STREQ(d.str("%Y").c_str(), "1970");
    ASSERT_STREQ(d.str("").c_str(), "");
}


